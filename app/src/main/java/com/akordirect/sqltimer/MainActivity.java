package com.akordirect.sqltimer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private CountDownTimer countDownTimer;
    private TextView textView;
    private TextView textViewYourFeelings;
    private Button buttonSaveInDb;
    private EditText editTextFeelings;
    private SQLiteDatabase database;

    List<String> feelings = new ArrayList<>();


    private int timerIsRunning;

    @Override // 1
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate - start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // dm
        textView = findViewById(R.id.textView_main_dm);
        textViewYourFeelings = findViewById(R.id.textView_how_your_feelings);
        buttonSaveInDb = findViewById(R.id.button_save_in_DB);
        editTextFeelings = findViewById(R.id.editText_feelings);

        database = this.openOrCreateDatabase("dm_test", MODE_PRIVATE, null);
        String sql = "create table if not exists users (name varchar, age int(3))";
        database.execSQL(sql);

        buttonSaveInDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFeelingsInDb(view);
            }
        });

        //int tt = 3600000; // equals 1 hour

        if (savedInstanceState != null) {
            timerIsRunning = savedInstanceState.getInt("qqKey");
            Log.d(TAG, "timerIsRunning = " + timerIsRunning);
        } else {
            timerIsRunning = 0;
            Log.d(TAG, "timerIsRunning = " + timerIsRunning);
        }

        if (timerIsRunning == 0) {
            Log.i(TAG, "CountDownTimer is created!");
            countDownTimer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long l) {
                    textView.setText("Timer is " + l / 1000);
                    Log.d(TAG, "Timer is " + l / 1000);
                }

                @Override
                public void onFinish() {
                    doWhenTimeIsFinished();
                    timerIsRunning = 0;
                    Log.d(TAG, "timerIsRunning start equals " + timerIsRunning);
                }

            };
            countDownTimer.start();
            Log.d(TAG, "countDownTimer is started!!!");

            buttonSaveInDb.setVisibility(View.VISIBLE);
            editTextFeelings.setVisibility(View.VISIBLE);

            timerIsRunning = 1;
        } else {
            buttonSaveInDb.setVisibility(View.INVISIBLE);
            editTextFeelings.setVisibility(View.INVISIBLE);
            textViewYourFeelings.setText("Ok, Thanks! Inf is saved in DB.");
            Log.i(TAG, "CountDownTimer is NOT created!");
        }

        Log.d(TAG, "onCreate - Finish");
    }


    public void saveFeelingsInDb(View view) {


//        var dispose = dataSourse()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                        buttonTest.setText("new int $it")
//                        Log.i(TAG, "new data $it")
//                }, { }, { })


        Log.d(TAG, "Data is saved in Data Base!");
        String stringCondition = editTextFeelings.getText().toString();
        String sql = "insert into users (name, age) values ('" + stringCondition + "', 1)";
        database.execSQL(sql);

        buttonSaveInDb.setVisibility(View.INVISIBLE);
        editTextFeelings.setVisibility(View.INVISIBLE);
        textViewYourFeelings.setText("Ok, Thanks! Inf is saved in DB.");

        pringFeelingsFromDB();
    }


    private void doWhenTimeIsFinished() {
        textView.setText("Time is finished!");
        Log.d(TAG, "Ok. All is good!");

        Toast.makeText(MainActivity.this, "Game: Timer is finished", Toast.LENGTH_SHORT).show();

        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MainActivity.this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(MainActivity.this, "channel01")
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Test")
                .setContentText("You see me!")
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)   // heads-u
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.this);
        notificationManager.notify(0, notification);

//        buttonSaveInDb.setVisibility(View.VISIBLE);
//        editTextFeelings.setVisibility(View.VISIBLE);
    }


    private void pringFeelingsFromDB() {
        try {
            database = this.openOrCreateDatabase("dm_test", MODE_PRIVATE, null);
            String sql = "create table if not exists users (name varchar, age int(3))";
//            String sql = "drop table users";
//            Log.d(TAG, "request in DB: " + sql);
//            database.execSQL(sql);

//            String sql = "create table if not exists newusers (_id INTEGER(3) primarykey, name varchar, age int(3))";
            Log.d(TAG, "request in DB: " + sql);
            database.execSQL(sql);
//    sql = "insert into users (name, age) values ('Dmitry', 38)";
//    database.execSQL(sql);


//    sql = "delete from users where name ='Dave')"; // delete
//    database.execSQL(sql);


            Cursor c = database.rawQuery("select * from users", null);
            int nameIndex = c.getColumnIndex("name");
            int ageIndex = c.getColumnIndex("age");

            if (c != null) {
                c.moveToFirst();

                do {
                    String nameFromDB = c.getString(nameIndex);
                    Log.i(TAG, "nameIndex = " + nameFromDB + " - " + c.getString(ageIndex));
                    feelings.add(nameFromDB);
                } while (c.moveToNext());
            }
            for (String feeling : feelings) {
                Log.d(TAG, feeling);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error with Db: " + e.getMessage());
        }
    }

    @Override //2
    protected void onStart() {
        Log.d(TAG, "OnStart");
        super.onStart();
    }

    @Override // 3
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override // 4
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume");
    }


    @Override // -1
    protected void onPause() {
        Log.d(TAG, "OnPause");
        super.onPause();
    }

    @Override// -2
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("ttKey", "1");
        outState.putInt("qqKey", timerIsRunning);
        Log.d(TAG, "onSaveInstanceState (DM)");
    }

    @Override //-3
    protected void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();
    }


    @Override // -4
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "OnDestroy (MainActivity)");
        super.onDestroy();
    }

}
